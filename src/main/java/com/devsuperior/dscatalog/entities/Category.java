package com.devsuperior.dscatalog.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Entity
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "categories")
public class Category implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Setter
  private String name;

  public Category(Long id, String name) {
	this.id = id;
	this.name = name;
  }

  @Column(columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private Instant createdAt;

  @Column(columnDefinition = "TIMESTAMP WITHOUT TIME ZONE")
  private Instant updatedAt;

  @ManyToMany(mappedBy = "categories")
  private Set<Product> products = new HashSet<>();

  @PrePersist
  public void prePersist() {
	createdAt = Instant.now();
  }

  @PreUpdate
  public void preUpdate() {
	updatedAt = Instant.now();
  }

}
