package com.devsuperior.dscatalog.resources;

import com.devsuperior.dscatalog.dto.ProductDTO;
import com.devsuperior.dscatalog.services.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductResources {

  private final ProductService service;

  @GetMapping
  public ResponseEntity<Page<ProductDTO>> findAll(
          @RequestParam(value = "categoryId", defaultValue = "0") Long categoryId,
          @RequestParam(value = "name", defaultValue = "") String name,
          final Pageable page) {
	var categories = service.findAllPaged(name, categoryId, page);
	return ResponseEntity.ok().body(categories);
  }

  @GetMapping("/{id}")
  public ResponseEntity<ProductDTO> findById(@PathVariable("id") Long id) {
	var product = service.findById(id);
	return ResponseEntity.ok().body(product);
  }

  @PostMapping
  public ResponseEntity<ProductDTO> save(@Valid @RequestBody ProductDTO productDTO) {
	var dto = service.save(productDTO);
	var uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId()).toUri();
	return ResponseEntity.created(uri).body(dto);
  }

  @PutMapping("/{id}")
  public ResponseEntity<ProductDTO> update(@PathVariable("id") Long id, @Valid @RequestBody ProductDTO productDTO) {
	var dto = service.update(id, productDTO);
	return ResponseEntity.ok().body(dto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<ProductDTO> delete(@PathVariable("id") Long id) {
	service.delete(id);
	return ResponseEntity.noContent().build();
  }

}
