package com.devsuperior.dscatalog.resources.exceptions;

import lombok.*;

import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StandardError implements Serializable {

  private Instant timestamp;
  private int status;
  private String error;
  private String message;
  private String path;

}
