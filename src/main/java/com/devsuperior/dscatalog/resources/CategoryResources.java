package com.devsuperior.dscatalog.resources;

import com.devsuperior.dscatalog.dto.CategoryDTO;
import com.devsuperior.dscatalog.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryResources {

  private final CategoryService service;

  @GetMapping
  public ResponseEntity<Page<CategoryDTO>> findAll(Pageable page) {
	var categories = service.findAllPaged(page);
	return ResponseEntity.ok().body(categories);
  }

  @GetMapping("/{id}")
  public ResponseEntity<CategoryDTO> findById(@PathVariable("id") Long id) {
	var category = service.findById(id);
	return ResponseEntity.ok().body(category);
  }

  @PostMapping
  public ResponseEntity<CategoryDTO> save(@RequestBody CategoryDTO categoryDTO) {
	var dto = service.save(categoryDTO);
	var uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(dto.getId()).toUri();
	return ResponseEntity.created(uri).body(dto);
  }

  @PutMapping("/{id}")
  public ResponseEntity<CategoryDTO> update(@PathVariable("id") Long id, @RequestBody CategoryDTO categoryDTO) {
	var dto = service.update(id, categoryDTO);
	return ResponseEntity.ok().body(dto);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<CategoryDTO> delete(@PathVariable("id") Long id) {
	service.delete(id);
	return ResponseEntity.noContent().build();
  }

}
